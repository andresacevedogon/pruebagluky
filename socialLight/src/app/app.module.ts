import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {OptionsDrawerComponent} from './ThemeOptions/options-drawer/options-drawer.component';
import {DevToolsExtension, NgRedux, NgReduxModule} from "@angular-redux/store";
import {rootReducer, ArchitectUIState} from './ThemeOptions/store';
import {ConfigActions} from './ThemeOptions/store/config.actions';
import {AppRoutingModule} from "./app-routing.module";
import {HttpClientModule} from "@angular/common/http";
import {ToastrModule} from "ngx-toastr";
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {PERFECT_SCROLLBAR_CONFIG} from 'ngx-perfect-scrollbar';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';

//Componentes
import {ThemeOptions} from "./theme-options";
import {LoadingBarRouterModule} from "@ngx-loading-bar/router";
import {AppComponent} from './app.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ComponentsModule} from "./components/components.module";
import {LoginComponent} from './login/login.component';
import {LoginStoreService} from "./login/login.store.service";
import {RedsocialComponent} from "./redsocial/redsocial.component";
import {AngularFontAwesomeModule} from "angular-font-awesome";
import {BienvenidaComponent} from './bienvenida/bienvenida.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [
    AppComponent,
    OptionsDrawerComponent,
    DashboardComponent,
    LoginComponent,
    RedsocialComponent,
    BienvenidaComponent,
  ],
  imports: [
    AppRoutingModule,
    HttpClientModule,
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgReduxModule,
    LoadingBarRouterModule,
    ComponentsModule,
    ToastrModule.forRoot(),
    PerfectScrollbarModule,
    AngularFontAwesomeModule,
    NgbModule
  ],
  providers: [
    ConfigActions,
    ThemeOptions,
    LoginStoreService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private ngRedux: NgRedux<ArchitectUIState>,
              private devTool: DevToolsExtension) {

    this.ngRedux.configureStore(
      rootReducer,
      {} as ArchitectUIState,
      [],
      [devTool.isEnabled() ? devTool.enhancer() : f => f]
    );

  }
}
