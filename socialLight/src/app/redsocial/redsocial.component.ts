import {Component, OnInit} from '@angular/core';
import {RedsocialService} from "./redsocial.service";
import {Post, User} from "./redsocialEnity";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-redsocial',
  templateUrl: './redsocial.component.html',
  styleUrls: ['./redsocial.component.sass'],
  providers: [RedsocialService]
})
export class RedsocialComponent implements OnInit {

  postsData: Array<Post>;
  usersData: Array<User>;
  imagen = null;
  imgWidth;
  foto: File = null;

  constructor(private redSocialService: RedsocialService, private modalService: NgbModal, public toastr: ToastrService) {
  }

  ngOnInit() {

    /**
     * @desc Recurso que consulta al API los post y se almacena en una variable
     * para mostrar en la vista
     */
    this.redSocialService.getAllPosts().subscribe((res: any) => {
      this.postsData = res.result;
    });

    /**
     * @desc Recurso que consulta al API los usuarios y se almacena en una variable
     * para mostrar en la vista
     */
    this.redSocialService.getAllUsers().subscribe((res: any) => {
      this.usersData = res.result;
    });
  }

  /**
   * @name onFileFoto
   * @desc Metodo que captura la foto ingresada y la convierte a base64 para mostrar y la guarda en formato
   *      para enviar al API
   * @param files
   */
  onFileFoto(files: FileList) {
    let foto = files.item(0);
    let reader = new FileReader();
    reader.onload = (event: any) => {
      this.imagen = event.target.result; //Base64 para mostrar
      var img = new Image();
      img.onload = () => {
        this.imgWidth = img.width;
      };
      img.src = event.target.result;
    };

    this.foto = foto; //File a enviar al API
    reader.readAsDataURL(foto);
  }

  /**
   * @desc Metodo que realiza una publicacion nueva en el feed
   * @param comment
   */
  upPublish(comment) {
    console.log('post: ' + comment + 'imagen: ' + this.foto);
    this.imagen = null;
    this.modalService.dismissAll('publish');
    this.toastr.success('¡Huy que bien!!! Ahora todos pueden ver tu publicación.');
  }

  /**
   * @desc Metodo que realiza una publicacion de la historia de un usuario
   * @param comment
   */
  publishCommentHistory(comment) {
    console.log('post: ' + comment);
    this.modalService.dismissAll('historyPost');
    this.toastr.success('Tú publicación se realizó con exito.');
  }

  /**
   * @desc Metodo que realiza una publicacion sobre un post de un usuario
   * @param comment
   */
  publishComment(post: string) {
    console.log(post);
    this.toastr.success('Tú publicación se realizó con exito.');
  }

  /**
   * @desc Metodo que abre una modal
   * @param content
   */
  openModal(content) {
    if (content._def.references.historyPost) {
      this.modalService.open(content);
    } else {
      this.modalService.open(content, {backdrop: 'static', size: "lg"});
    }
  }

}
