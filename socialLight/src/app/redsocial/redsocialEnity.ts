export interface User {
  first_name: string,
  last_name: string,
  gender: string,
  dob: string,
  email: string,
  phone: string,
  website: string,
  address: string,
  status: string
}

export interface Post {
  user_id: string,
  title: string,
  body: string
}
