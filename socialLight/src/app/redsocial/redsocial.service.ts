import {Injectable} from '@angular/core';
import { map } from 'rxjs/operators';
import {LoginStoreService} from "../login/login.store.service";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class RedsocialService {

  private pathUrl = ['https://gorest.co.in', 'public-api'].join('/');

  constructor(private http: HttpClient, private token: LoginStoreService) {
  }

  /**
   * @name getAllPosts
   * @desc método que trae todos los usuarios del api
   */
  getAllUsers() {
    const url = this.pathUrl + '/users?' + 'access-token=6rGfrCBF2MTOq7vOPswIkMtzzcSDsEIoBzPA';
    return this.http.get(url);
  }

  /**
   * @name getAllPosts
   * @desc método que trae todos los posts del api
   */
  getAllPosts() {
    const url = this.pathUrl + '/posts?' + 'access-token=6rGfrCBF2MTOq7vOPswIkMtzzcSDsEIoBzPA';
    return this.http.get(url);
  }
}
