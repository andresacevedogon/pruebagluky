import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {LoginStoreService} from "./login.store.service";
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass'],
  providers: [LoginStoreService]
})
export class LoginComponent implements OnInit {

  accountForm: FormGroup; //Formulario

  constructor(public token: LoginStoreService, private router: Router, public toastr: ToastrService) {
  }

  ngOnInit() {
    //Se crea el formulario del login
    this.accountForm = new FormGroup({
      user: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }

  /**
   * @desc Metodo que valida el usuario y añade el token en un servicio para propagar en la aplicacion
   */
  onSumbit() {
    let form = this.accountForm.value;
    if (form.user === 'andres' && form.password === '123456') {
      this.token.tokenService = '6rGfrCBF2MTOq7vOPswIkMtzzcSDsEIoBzPA';
      //Se envia al dashboard
      this.router.navigate(['dashboard/bienvenido']);
      this.toastr.success('¡Que gusto tenerte de regreso!.');
    } else {
      this.toastr.error('El usuario o contraseña son incorrectos, intentelo de nuevo.');
    }
  }
}
