import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CommonModule} from "@angular/common";
import {BrowserModule} from "@angular/platform-browser";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {LoginComponent} from "./login/login.component";
import {RedsocialComponent} from "./redsocial/redsocial.component";
import {BienvenidaComponent} from "./bienvenida/bienvenida.component";

const routes: Routes = [
  {
    path: 'dashboard', component: DashboardComponent,
    children: [
      {path: '', redirectTo: 'bienvenido', pathMatch: 'full'},
      {
        path: 'bienvenido',
        component: BienvenidaComponent,
        data: {extraParameter: 'dashboardsMenu'}
      },
      {
        path: 'red-social',
        component: RedsocialComponent,
        data: {extraParameter: 'dashboardsMenu'}
      },
    ]
  },
  {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
  {
    path: 'login', component: LoginComponent
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule,
    BrowserModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
