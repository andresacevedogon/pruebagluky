import {Component} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'socialLight';

  constructor(private router: Router) {
    //Cuando se actualiza la pagina va al login
    this.router.navigate(['login']);
  }
}
